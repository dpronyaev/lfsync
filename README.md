**Build&Install:**

```
cd src/

sudo ./build
```


(it will build project and place binary file lfsync into /usr/local/bin)


**CLI args:**

```
--src=string source directory path (this argument is neccessary!)

--dst=string destination directory path (this argument is neccessary!)

--mthread=int number of theads to run in parallel (optional)

--silent do not show message about every single file

--overwrite overwrite existing files in destination directory

--dry not actualy copy any data

--help show this message
```

