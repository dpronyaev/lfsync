package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"
)

const version string = "v.0.1"

//data - struct fot all temporary data
type data struct {
	dstFiles, srcFiles, dstDirs, srcDirs []string
}

func main() {
	var wg sync.WaitGroup

	//initialize struct for data and WaitGroup for goroutine sync
	data := new(data)

	// parse flags from command line. src and dst are necessary.
	src := flag.String("src", "", "source path")
	dst := flag.String("dst", "", "destination path")
	dryrun := flag.Bool("dry", false, "dry run without actual copying")
	silent := flag.Bool("silent", false, "work without messages about every single file")
	mthread := flag.Int("mthread", 0, "number of threads in multithread mode")
	overwrite := flag.Bool("overwrite", false, "overwrite existing files in destination directories")
	help := flag.Bool("help", false, "show this message")
	flag.Parse()

	// show help
	if *help {
		showHelp()
	}

	// if necessary CLI args are missing
	if (*src == "" || *dst == "") && !*help {
		showWelcome()
		log.Fatal("You must specify source and desination dirs using --src and --dst keys.")
	}

	// cut "/" at the end of src and dst path
	if []byte(*src)[len([]byte(*src))-1] == '/' {
		*src = string([]byte(*src)[0 : len([]byte(*src))-1])
	}

	if []byte(*dst)[len([]byte(*dst))-1] == '/' {
		*dst = string([]byte(*dst)[0 : len([]byte(*dst))-1])
	}

	showWelcome()

	// list dirs and files
	getDirContent(*src, *dst, data, *silent)

	// create empty dirs in destination directory
	createDstDirs(data, *dryrun, *silent)

	if *mthread == 0 {
		copyFiles(data, *dryrun, *silent, *overwrite)
	} else {
		// initialize structs for safe update
		c := make(chan [2]string)
		// start goroutines
		for i := 0; i < *mthread; i++ {
			wg.Add(1)
			go mCopyFile(c, *dryrun, *silent, &wg)
		}
		for i := 0; i < len(data.srcFiles); i++ {
			if *overwrite || (!*overwrite && !exist(data.dstFiles[i])) {
				c <- [2]string{data.srcFiles[i], data.dstFiles[i]}
			} else if !*overwrite && exist(data.dstFiles[i]) {
				if !*silent {
					fmt.Println("FIle", data.dstFiles[i], "already exists. Skipping")
				}
			}
		}
		close(c)
		wg.Wait()
		fmt.Println("Job is done.")
	}
}

func showWelcome() {
	fmt.Println("Welcome to Look Forward Sync (lfsync)", version, "by Dmitry Pronyaev")
}

func showHelp() {
	showWelcome()
	fmt.Println("CLI args:")
	fmt.Println("--src=string source directory path (this argument is neccessary!)")
	fmt.Println("--dst=string destination directory path (this argument is neccessary!)")
	fmt.Println("--mthread=int number of theads to run in parallel (optional)")
	fmt.Println("--silent do not show message about every single file")
	fmt.Println("--overwrite overwrite existing files in destination directory")
	fmt.Println("--dry not actualy copy any data")
	fmt.Println("--help show this message")
	os.Exit(0)
}

func mCopyFile(c chan [2]string, dryrun bool, silent bool, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		select {
		// toDo[0] - source
		// toDo[1] - destination
		case toDo := <-c:
			if toDo[0] == "" {
				return
			}
			if !silent {
				fmt.Println(time.Now(), "Copying ", toDo[0], "to", toDo[1])
			}
			if !dryrun {
				in, readErr := os.Open(toDo[0])
				if readErr != nil {
					log.Fatal(readErr)
				}
				out, createErr := os.Create(toDo[1])
				if createErr != nil {
					log.Fatal(createErr)
				}
				_, writeErr := io.Copy(out, in)
				if writeErr != nil {
					log.Fatal(writeErr)
				}
				// do not forget to close file!
				out.Close()
				in.Close()

				// apply file permissions
				info, _ := os.Stat(toDo[0])
				mode := info.Mode()
				permErr := os.Chmod(toDo[1], mode)
				if permErr != nil {
					log.Fatal(permErr)
				}

			}
			if !silent {
				fmt.Println("Done.")
			}
		}
	}
}

func getDirContent(path string, dstPath string, data *data, silent bool) {
	pathPrefix, pathErr := filepath.Abs(path)
	if !silent {
		fmt.Println("Listing path", path)
	}
	if pathErr != nil {
		log.Fatal(pathErr)
	}

	dstPathPrefix := dstPath

	dirNum := 0
	fileNum := 0
	var nestedDirs []string
	var nestedDstDirs []string

	content, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	for _, current := range content {
		if current.IsDir() {
			dirString := pathPrefix + "/" + current.Name()
			dstDirString := dstPathPrefix + "/" + current.Name()
			data.srcDirs = append(data.srcDirs, dirString)
			data.dstDirs = append(data.dstDirs, dstDirString)
			nestedDirs = append(nestedDirs, dirString)
			nestedDstDirs = append(nestedDstDirs, dstDirString)
			dirNum++
		} else {
			fileString := pathPrefix + "/" + current.Name()
			dstFileString := dstPathPrefix + "/" + current.Name()
			data.srcFiles = append(data.srcFiles, fileString)
			data.dstFiles = append(data.dstFiles, dstFileString)
			fileNum++
		}
	}

	if len(nestedDirs) > 0 {
		for i := 0; i < len(nestedDirs); i++ {
			getDirContent(nestedDirs[i], nestedDstDirs[i], data, silent)
		}
	}
}

func createDstDirs(data *data, dryrun bool, silent bool) {
	for i := 0; i < len(data.srcDirs); i++ {
		if !silent {
			fmt.Println("Creating directory", data.dstDirs[i])
		}

		if !dryrun {
			info, _ := os.Stat(data.srcDirs[i])
			perm := info.Mode()
			err := os.MkdirAll(data.dstDirs[i], perm)
			if err != nil {
				log.Fatal(err)
			}
			if !silent {
				fmt.Println(data.dstDirs[i], perm, "created successfully")
			}
		}
	}
}

func copyFiles(data *data, dryrun bool, silent bool, overwrite bool) {
	fmt.Println("Files to copy:", len(data.srcFiles))
	for i := 0; i < len(data.srcFiles); i++ {
		//read file to buffer
		if !silent {
			fmt.Println(time.Now(), "Copying ", data.srcFiles[i], "to", data.dstFiles[i], "...")
		}
		// check if file already exists in destination dir
		if _, err := os.Stat(data.dstFiles[i]); os.IsNotExist(err) {

		}
		if !dryrun {
			if overwrite || (!overwrite && !exist(data.dstFiles[i])) {
				in, readErr := os.Open(data.srcFiles[i])
				if readErr != nil {
					log.Fatal(readErr)
				}
				out, createErr := os.Create(data.dstFiles[i])
				if createErr != nil {
					log.Fatal(createErr)
				}
				_, writeErr := io.Copy(out, in)
				if writeErr != nil {
					log.Fatal(writeErr)
				}
				// do not foget to close file!
				out.Close()
				in.Close()

				// apply file permissions
				info, _ := os.Stat(data.srcFiles[i])
				mode := info.Mode()
				permErr := os.Chmod(data.dstFiles[i], mode)
				if permErr != nil {
					log.Fatal(permErr)
				}
				if !silent {
					fmt.Println("Done.")
				}
			} else if !overwrite && exist(data.dstFiles[i]) {
				if !silent {
					fmt.Println("FIle", data.dstFiles[i], "already exists. Skipping")
				}
			}

		}

	}
	fmt.Println(time.Now(), "All files are copied.")
}

func exist(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

